---
  title: Plateforme
  description: Découvrez comment la plateforme GitLab peut aider les équipes à collaborer et à créer des logiciels plus rapidement.
  hero:
    note: La plateforme DevSecOps
    header: Alimentée par l'IA
    sub_header: La plus complète
    description: Développez plus rapidement des logiciels de meilleure qualité sur une seule plateforme pour l'ensemble du cycle de vie de livraison de vos logiciels
    image:
      src: /nuxt-images/platform/loop-shield-duo.svg
      alt: Le cycle de vie DevOps (planifier, coder, compiler, tester, livrer une release, déployer, exploiter et effectuer le suivi) est représenté par un symbole de l'infini chevauchant le bouclier de sécurité.
    button:
      text: Commencer un essai gratuit
      href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: En savoir plus sur la tarification
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero
  table_data:
    - name: "Planning"
      link: "/solutions/agile-delivery/"
      icon: "plan-alt-2"
      features:
        - name: "DevOps Reports"
          link: "https://docs.gitlab.com/ee/administration/analytics/dev_ops_reports"
        - name: "DORA Metrics"
          link: "/solutions/value-stream-management/dora/"
        - name: "Value Stream Management"
          link: "/solutions/value-stream-management/"
        - name: "Pages"
          link: "https://docs.gitlab.com/ee/user/project/pages/"
        - name: "Wiki"
          link: "https://docs.gitlab.com/ee/user/project/wiki/"
        - name: "Portfolio Management"
          link: "https://docs.gitlab.com/ee/topics/plan_and_track.html#portfolio-management"
        - name: "Team Planning"
          link: "https://docs.gitlab.com/ee/topics/plan_and_track.html#team-planning"
        - name: "Epic Summary"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
        - name: "Issue Summary"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
        - name: "Generate issue description"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#issue-description-generation"
        - name: "Discussion Summary"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
      replacement: Jira
    - name: "Source Code Management"
      link: "/solutions/source-code-management/"
      icon: "cog-code"
      features:
        - name: "Remote Development"
          link: "https://docs.gitlab.com/ee/user/project/remote_development/"
        - name: "Web IDE"
          link: "https://docs.gitlab.com/ee/user/project/web_ide/"
        - name: "GitLab CLI"
          link: "https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html"
        - name: "Code Review Workflow"
          link: "https://docs.gitlab.com/ee/user/project/merge_requests/"
        - name: "Code Suggestions"
          link: "https://docs.gitlab.com/ee/user/project/repository/code_suggestions/"
        - name: "Code Explanation"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-explanation-in-the-ide"
        - name: "Code Review Summary"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-review-summary"
        - name: "Test Generation"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#test-generation"
      replacement: GitHub
    - name: "Continuous Integration"
      link: "/solutions/continuous-integration/"
      icon: "automated-code-alt"
      features:
        - name: "Secrets Management"
          link: "https://docs.gitlab.com/ee/ci/secrets/"
        - name: "Review Apps"
          link: "https://docs.gitlab.com/ee/ci/review_apps/"
        - name: "Code Testing and Coverage"
          link: "https://docs.gitlab.com/ee/ci/testing/"
        - name: "Merge Trains"
          link: "https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html"
        - name: "Suggested Reviewers"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#suggested-reviewers"
        - name: "Merge Request Summary"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#merge-request-summary"
        - name: "Root Cause Analysis"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#root-cause-analysis"
        - name: "Discussion Summary"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
      replacement: Jenkins
    - name: "Artifact Registry"
      icon: "package-alt-2"
      features:
        - name: "Virtual Registry"
          link: "https://docs.gitlab.com/ee/user/packages/dependency_proxy/"
        - name: "Container Registry"
          link: "https://docs.gitlab.com/ee/user/packages/container_registry/"
        - name: "Helm Chart Registry"
          link: "https://docs.gitlab.com/ee/user/packages/helm_repository/#helm-charts-in-the-package-registry"
        - name: "Package Registry"
          link: "https://docs.gitlab.com/ee/user/packages/"
      replacement: JFrog
    - name: "Security"
      link: "/solutions/security-compliance/"
      icon: "secure-alt-2"
      features:
        - name: "Container Scanning"
          link: "https://docs.gitlab.com/ee/user/application_security/container_scanning/"
        - name: "Software Composition Analysis"
          link: "https://docs.gitlab.com/ee/user/application_security/dependency_scanning/"
        - name: "API Security"
          link: "https://docs.gitlab.com/ee/user/application_security/api_security/"
        - name: "Coverage-guided Fuzz Testing"
          link: "https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/"
        - name: "DAST"
          link: "https://docs.gitlab.com/ee/user/application_security/dast/"
        - name: "Code Quality"
          link: "https://docs.gitlab.com/ee/ci/testing/code_quality"
        - name: "Secret Detection"
          link: "https://docs.gitlab.com/ee/user/application_security/secret_detection/"
        - name: "SAST"
          link: "https://docs.gitlab.com/ee/user/application_security/sast/"
        - name: "Vulnerability Explanation"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#vulnerability-explanation"
        - name: "Vulnerability Resolution"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#vulnerability-resolution"
      replacement: Snyk
    - name: "Compliance"
      icon: "protect-alt-2"
      link: "/solutions/continuous-software-compliance/"
      features:
        - name: "Release Evidence"
          link: "https://docs.gitlab.com/ee/user/project/releases/release_evidence"
        - name: "Compliance Management"
          link: "https://docs.gitlab.com/ee/administration/compliance.html"
        - name: "Audit Events"
          link: "https://docs.gitlab.com/ee/administration/audit_events.html"
        - name: "Software Bill of Materials"
          link: "/solutions/supply-chain/"
        - name: "Dependency Management"
          link: "https://docs.gitlab.com/ee/user/application_security/dependency_list/"
        - name: "Vulnerability Management"
          link: "https://docs.gitlab.com/ee/user/application_security/security_dashboard/"
        - name: "Security Policy Management"
          link: "https://docs.gitlab.com/ee/user/application_security/policies/"
    - name: "Continuous Delivery"
      icon: "continuous-delivery-alt"
      link: "/solutions/continuous-integration/"
      features:
        - name: "Release Orchestration"
          link: "https://docs.gitlab.com/ee/user/project/releases/"
        - name: "Infrastructure as Code"
          link: "https://docs.gitlab.com/ee/user/infrastructure/iac/index.html"
        - name: "Feature Flags"
          link: "https://docs.gitlab.com/ee/operations/feature_flags.html"
        - name: "Environment Management"
          link: "https://docs.gitlab.com/ee/ci/environments/"
        - name: "Deployment Management"
          link: "https://docs.gitlab.com/ee/topics/release_your_application.html"
        - name: "Auto DevOps"
          link: "/solutions/delivery-automation/"
      replacement: Harness
    - name: "Observability"
      icon: "monitor-alt-2"
      link: "/solutions/analytics-and-insights/"
      features:
        - name: "Service Desk"
          link: "https://docs.gitlab.com/ee/user/project/service_desk/"
        - name: "On-call Schedule Management"
          link: "https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html"
        - name: "Incident Management"
          link: "https://docs.gitlab.com/ee/operations/incident_management/"
        - name: "Error Tracking"
          link: "https://docs.gitlab.com/ee/operations/error_tracking.html"
        - name: "Product Analytics Visualization"
          link: "https://docs.gitlab.com/ee/user/analytics/"
        - name: "Value Stream Forecasting"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#value-stream-forecasting"
        - name: "AI Product Analytics"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#product-analytics"
      replacement: Sentry
  benefits:
    title: One platform
    subtitle: to empower Dev, Sec, and Ops teams
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: code source image
    is_accordion: true
    tabs:
      - tabButtonText: Development
        data_ga_name: development
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Flux de travail alimentés par l'IA
              text: Améliorez l'efficacité de vos processus et réduisez les durées de cycle de chaque utilisateur à l'aide de l'IA à chaque phase du cycle de vie du développement logiciel, de la planification et de la création de code aux tests, à la sécurité et à la surveillance
              headerCtas:
                - externalUrl: /gitlab-duo/
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
              ctaHeader: "See it in action:"
              ctas:
                - externalUrl: https://player.vimeo.com/video/855805049?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/894621401?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: Code Suggestions
                  data_ga_name: Code Suggestions
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Application unique
              text: GitLab regroupe toutes les fonctionnalités DevSecOps dans une seule application avec un magasin de données unifié pour que tout soit au même endroit.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s use of DORA metrics video
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s Value Streams Dashboard video
                  data_ga_name: GitLab’s Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Amélioration de la productivité
              text: L'application unique de GitLab offre une meilleure expérience à l'utilisateur, ce qui améliore le durée de cycle et permet d'éviter le changement de contexte.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925629920?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s Portfolio Management
                  data_ga_name: GitLab’s Portfolio Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925632272?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s OKR Management
                  data_ga_name: GitLab’s OKR Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925633691?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: Design Uploads to GitLab issues
                  data_ga_name: Design Uploads to GitLab issues
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Une meilleure automatisation
              text: Les outils d'automatisation de GitLab sont plus fiables et plus riches en fonctionnalités, ce qui permet d'éliminer la charge cognitive et les tâches inutiles.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023715?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s CD Overview
                  data_ga_name: GitLab’s CD Overview
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://docs.gitlab.com/ee/operations/error_tracking.html
                  text: Error tracking documentation
                  data_ga_name: Error tracking documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Incident management documentation
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            quote: |
              **La vision de GitLab, qui consiste à allier la stratégie à la portée du logiciel et au code est très puissante.** J'apprécie les efforts majeurs que GitLab ne cesse de fournir dans sa plateforme.”
            author: Jason Monoharan.
            authorTitle: Vice-président de la technologie
            authorCompany: Nisi anim exercitation qui irure.
            authorHeadshot: https://images.ctfassets.net/xz1dnu24egyd/1G9lo7UyVxvW6m1CTNND9b/7b8b0449fc8ae1299e3b4fc56f4dd363/JasonManoharan.png
            companyLogo: https://images.ctfassets.net/xz1dnu24egyd/2DKPuM0RDt2jdfzIaTroKS/1aeee8ed2e7939523ef89e7046d5a468/iron-mountain.svg
            cta:
              externalUrl: '/customers/iron-mountain/'
              text: En savoir plus
              dataGaName: iron mountain case study
              dataGaLocation: body
            stats:
              - header: $150k
                text: économies de coûts réalisées par an en moyenne
              - header: 20 heures
                text: gain de temps d'intégration par projet
          cta:
            title: Harness the power of AI with
            highlight: GitLab Duo
            text: Learn more
            externalUrl: /gitlab-duo/
            data_ga_name: GitLab Duo
            data_ga_location: body
            additionalIcon: true
      - tabButtonText: Sécurité
        data_ga_name: security
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: La sécurité est intégrée, elle n'est pas greffée
              text: Les fonctionnalités de sécurité de GitLab (DAST, test à données aléatoires, analyse des conteneurs et vérification des API) sont intégrées de bout en bout.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925635707?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: Dynamic Application Security Testing (DAST) video
                  data_ga_name: Dynamic Application Security Testing (DAST) video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925676815?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: Container scanning video
                  data_ga_name: Container scanning video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925677603?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: API security and web API Fuzzing video
                  data_ga_name: API security and web API Fuzzing video
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Conformité et gestion précise des politiques
              text: GitLab offre une solution de gouvernance complète permettant la séparation des tâches entre les équipes. L'éditeur de politique de GitLab permet des règles d'approbation personnalisées adaptées aux exigences de conformité de chaque organisation, réduisant ainsi les risques
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/administration/compliance.html
                  text: Compliance Management documentation
                  data_ga_name: Compliance Management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://www.youtube.com/embed/8kQgxgseFU0?si=KSQOg_0ImMjirxJR
                  text: GitLab’s Compliance Frameworks video
                  data_ga_name: GitLab’s Compliance Frameworks
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925679982?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s Requirements Management video
                  data_ga_name: GitLab’s Requirements Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Automatisation de la sécurité
              text: Les outils d'automatisation avancés de GitLab permettent de gagner en rapidité grâce à des garde-fous qui garantissent que le code est automatiquement analysé pour détecter les vulnérabilités.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925680640?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s Security Dashboard video
                  data_ga_name: GitLab’s Security Dashboard
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
          case_study:
            quote: |
              **GitLab is helping us catch security flaws early and it's integrated it into the developer's flow.** An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.”
            author: Mitch Trale,
            authorTitle: Head of Infrastructure
            authorCompany: Nisi anim exercitation qui irure.
            companyLogo: https://images.ctfassets.net/xz1dnu24egyd/4mEBdmMQrpjOn1VJ2idd9h/39fbbf0fc290dda417a35d1e437de4a3/hackerone.svg
            cta:
              externalUrl: '/customers/hackerone/'
              text: Read the study
              dataGaName: hackerone
              dataGaLocation: hackerone
            stats:
              - header: 7.5x
                text: faster pipeline time
              - header: 4 hours
                text: development time per engineer saved/weekly
          cta:
            title: See how to add security scans to your
            highlight: CI pipeline
            text: Launch demo
            data_ga_name: ci pipeline
            data_ga_location: body
            modal: true
            iconName: slp-laptop-video
            demo:
              subtitle: Add security scans to your CI/CD Pipeline
              video:
                url: https://capture.navattic.com/clq78b76l001b0gjnbxbd5k1f
              scheduleButton:
                text: Schedule a custom demo
                href: https://about.gitlab.com/sales/
                data_ga_name: demo
                data_ga_location: body
      - tabButtonText: Opérations
        data_ga_name: operations
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Faites évoluer les charges de travail de votre entreprise
              text: GitLab prend facilement en charge l'entreprise à n'importe quelle échelle avec la possibilité de gérer et de mettre à niveau avec un temps d'arrêt quasi nul.
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/user/infrastructure/iac/
                  text: Infrastructure as code (IaC) documentation
                  data_ga_name: Infrastructure as code (IaC) documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Incident management documentation
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Visibilité inégalée des métriques
              text: Le magasin de données unifié de GitLab fournit des analyses pour l'ensemble du cycle de vie du développement logiciel en un seul endroit, éliminant ainsi le besoin d'intégrations de produits supplémentaires.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s use of DORA metrics video
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab’s Value Streams Dashboard video
                  data_ga_name: GitLab’s Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Pas d'enfermement du Cloud
              text: GitLab n'est pas lié commercialement à un seul fournisseur de services Cloud, ce qui élimine le risque d'enfermement propriétaire.
              ctas:
                - externalUrl: https://about.gitlab.com/topics/multicloud/
                  text: Multicloud documentation
                  data_ga_name: Multicloud documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://about.gitlab.com/solutions/gitops/
                  text: GitOps documentation
                  data_ga_name: GitOps documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Lower Total Cost of Ownership
              text: ''
              ctas:
                - description: 'Learn how the world’s largest defence contractor uses GitLab to shrink toolchains, speed production, and improve security:'
                  externalUrl: https://about.gitlab.com/customers/lockheed-martin/
                  text: Lockheed Martin case study
                  data_ga_name: Lockheed Martin case study
                  data_ga_location: body
                  iconName: gl-doc-text
                - description: 'Learn how CARFAX trimmed their DevSecOps toolchain and improved security with GitLab:'
                  externalUrl: https://about.gitlab.com/customers/carfax/
                  text: CARFAX case study
                  data_ga_name: CARFAX case study
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            quote: |
              **Improved development and delivery efficiency by over 87%,resulting in over $23 million in savings.** GitLab enabled the organizations to drastically reduce time spent across each phase of the entire DevOps lifecycle.”
            companyLogo: https://images.ctfassets.net/xz1dnu24egyd/rdWpo3pKJBUFLzDFmTNIE/d69e61550635ebba0556216c44d401a7/forrester-logo.svg
            cta:
              externalUrl: 'https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html'
              text: Read the study
              dataGaName: resources study forrester
              dataGaLocation: body
            stats:
              - header: $200.5M
                text: total benefits over three years
              - header: 427%
                text: total return on investment (ROI)
          cta:
            title: How much is your toolchain
            highlight: costing you?
            text: Try our ROI Calculator
            externalUrl: /calculator/
            data_ga_name: try our roi calculator
            data_ga_location: body
  video_spotlight:
    header: Want to increase velocity? Consolidate your toolchain today.
    list:
      - text: Improve collaboration
        iconName: gl-check
      - text: Reduce admin burden
        iconName: gl-check
      - text: Increase security
        iconName: gl-check
      - text: Lower total cost of ownership
        iconName: gl-check
      - text: Scale seamlessly
        iconName: gl-check
    blurb: |
      **Don’t know where to start?**
      Our sales team can help guide you.
    video:
      title: Why Gitlab
      url: https://player.vimeo.com/video/799236905?h=4eee39a447
      text: Learn More
      data_ga_name: Learn More
      data_ga_location: body
      image: https://images.ctfassets.net/xz1dnu24egyd/7KcXktuJTizzgMwF5o8n66/eddd4708263c41d1afa49399222d8f55/platform-video.png
    button:
      externalUrl: /sales/
      text: Talk to sales
      data_ga_name: sales
      data_ga_location: body
  recognition:
    heading: Industry leaders trust GitLab
    subtitle: GitLab ranks as a G2 Leader across DevOps categories.
    badges:
          - src: https://images.ctfassets.net/xz1dnu24egyd/712lPKncEMDkCErGOD91KQ/b3632635c9e3bf13a2d02ada6374029b/DevOpsPlatforms_Leader_Leader.png
            alt: G2 - Winter 2024 - Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/17EE9DHTQpLyGbJAQslSEk/e04bb1e6e71d895af94d10ffb85471e3/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 - Winter 2024 - Easiest to Use
          - src: https://images.ctfassets.net/xz1dnu24egyd/1SNrMzYNTrOvEnOuNucAMD/be22fc71855a96d9c05627c7085148a9/users-love-us.png
            alt: G2 - Winter 2024 - Users love us
          - src: https://images.ctfassets.net/xz1dnu24egyd/3UhXS8fSwKieddbliKZEGn/9bd6e04223769aa0869470befd78dc62/ValueStreamManagement_BestUsability_Total.png
            alt: G2 - Winter 2024 - Best Usability
          - src: https://images.ctfassets.net/xz1dnu24egyd/7AODDnwkskODEsylwXkBys/908eb94a40d8ab2f64bdcacbd18f35a3/DevOpsPlatforms_Leader_Europe_Leader.png
            alt: G2 - Winter 2024 - Leader Europe
          - src: https://images.ctfassets.net/xz1dnu24egyd/4xk6CmKqUndyuir0m4rF5b/b069fcc665b3b11c2a017fb36aa55130/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 - Winter 2024 - Leader Enterprise
          - src: https://images.ctfassets.net/xz1dnu24egyd/7jG9DylTxCnElENurGpocF/4241ca192ba6ec8c1d73d07c5a065d76/DevOpsPlatforms_MomentumLeader_Leader.png
            alt: G2 - Winter 2024 - Momentum Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/I7XhKHEf8rB7o1zYwYyBf/0b62ae05ecb749e83cd453de13972482/DevOps_Leader_Americas_Leader.png
            alt: G2 - Winter 2024 - Leader Americas
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'GitLab est reconnu comme un leader dans le Gartner® Magic Quadrant™ 2023 dédié aux plateformes DevOps'
        link:
          text: Read the report
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "GitLab est reconnu comme unique leader dans le rapport The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2\_2023"
        link:
          text: Lire le rapport
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
  pricing:
    header: Découvrez le forfait qui convient le mieux à votre équipe
    ctas:
      - button:
          externalUrl: /pricing/premium/
          text: Why GitLab Premium?
          data_ga_name: why gitlab premium
          data_ga_location: body
      - button:
          externalUrl: /pricing/ultimate/
          text: Why GitLab Ultimate?
          data_ga_name: why gitlab ultimate
          data_ga_location: body
