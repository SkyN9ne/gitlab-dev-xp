---
  title: Sécurité de la chaîne d'approvisionnement logicielle
  description: Sécurisez votre chaîne d'approvisionnement logicielle, gardez une longueur d'avance sur les vecteurs de menace et établissez des politiques pour faciliter le respect de la conformité afin de fournir plus rapidement des logiciels sécurisés.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  Automatisation intégrée et application des politiques
        title: Sécurité de la chaîne d'approvisionnement logicielle
        subtitle: Sécurisez votre chaîne d'approvisionnement logicielle, gardez une longueur d'avance sur les vecteurs de menace et établissez des politiques pour faciliter le respect de la conformité afin de fournir plus rapidement des logiciels sécurisés.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Essayer Ultimate gratuitement
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: En savoir plus sur la tarification
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/resources/resources_11.jpeg
          image_url_mobile: /nuxt-images/supply-chain-hero.png
          alt: "Image: sécurité de la chaîne d'approvisionnement logicielle"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Adoptée par
        logos:
          - name: Bendigo and Adelaide Bank Logo
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Lien vers l'étude de cas Bendigo and Adelaide Bank
          - name: Hackerone Logo
            image: /nuxt-images/logos/hackerone-logo.png
            url: /customers/hackerone/
            aria_label: Lien vers l'étude de cas Hackerone
          - name: new10 Logo
            image: /nuxt-images/logos/new10.svg
            url: /customers/new10/
            aria_label:  Lien vers l'étude de cas New10
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: Lien vers l'étude de cas The Zebra
          - name: Chorus Logo
            image: /nuxt-images/logos/chorus.svg
            url: /customers/chorus/
            aria_label: Lien vers l'étude de cas Chorus
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: Lien vers l'étude de cas Hilti
    - name: 'side-navigation-variant'
      links:
        - title: Présentation
          href: '#overview'
        - title: Capacités
          href: '#capabilities'
        - title: Clients
          href: '#customers'
        - title: Tarifs
          href: '#pricing'
        - title: Ressources
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Sécurisez votre chaîne d'approvisionnement logicielle de bout en bout
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: Icône d'intégration continue
                      variant: marketing
                    header: Protégez le cycle de développement logiciel
                    text: Protégez plusieurs surfaces d'attaque, y compris votre code, votre compilation, vos dépendances et vos artefacts de release
                    link_text: En savoir plus sur DevSecOps
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                    header: Respectez les exigences de conformité
                    text: Accès facile aux rapports d'audit et de gouvernance
                    link_text: Pourquoi utiliser GitLab
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: release
                      alt: Icône de bouclier avec une coche
                      variant: marketing
                    header: Mettez en place des garde-fous
                    text: Contrôlez l'accès et mettez en place des politiques
                    link_text: En savoir plus sur notre approche de plateforme
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              data:
                video:
                  url: 'https://player.vimeo.com/video/762685637?h=f96e969756'
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Codez, compilez, livrez une release. En toute sécurité.
                sub_description: ''
                white_bg: true
                markdown: true
                sub_image: /nuxt-images/solutions/supply-chain.png
                solutions:
                  - title: Établissez un modèle Zero Trust
                    description: |
                      La gestion des identités et des accès (IAM) est l'un des plus grands vecteurs d'attaque dans la chaîne d'approvisionnement logicielle. Sécurisez l'accès à GitLab en authentifiant, en autorisant et en validant en permanence toutes les identités humaines et machines opérant dans votre environnement.
                      *  Mettez en œuvre un [contrôle d'accès](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html) granulaire, notamment une [authentification à deux facteurs](https://docs.gitlab.com/ee/security/two_factor_authentication.html)
                      *  Établissez des [politiques d'expiration des jetons](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
                      *  Mettez en place des [politiques](https://docs.gitlab.com/ee/administration/compliance.html#policy-management) conformément aux règles organisationnelles ou réglementaires
                      *  Générez des [rapports d'audit et de gouvernance](https://docs.gitlab.com/ee/administration/audit_reports.html) complets pour le respect de la conformité
                      *  Faites respecter [les approbations par deux personnes](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html) pour des garde-fous supplémentaires
                  - title: Sécurisez votre code source
                    description: |
                      Garantissez la sécurité et l'intégrité de votre code source en gérant les personnes qui ont accès au code et la manière dont les modifications apportées au code sont examinées et fusionnées.

                      * Mettez en place un contrôle des versions, un [historique du code](https://docs.gitlab.com/ee/user/project/repository/git_history.html?_gl=1*1ngzpgw*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MjQ3LjAuMC4w) et un [contrôle d'accès](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html) à votre code source
                      *  Utilisez des tests automatisés de [qualité du code](https://docs.gitlab.com/ee/ci/testing/code_quality.html) pour analyser l'impact des modifications sur les performances
                      *  Faites respecter les [règles de revue de code et d'approbation](https://docs.gitlab.com/ee/ci/testing/code_quality.html) pour contrôler ce qui entre en production
                      *  Exécutez [des analyses de sécurité automatisées](https://docs.gitlab.com/ee/user/application_security/) pour détecter les vulnérabilités avant que votre code ne soit fusionné
                      *  Assurez-vous que les mots de passe et les informations sensibles ne se trouvent pas dans votre code source grâce à la [détection des secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/) automatisée 
                      *  Mettez en place des [validations signées](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/) pour empêcher l'emprunt d'identité des développeurs
                  - title: Dépendances sécurisées
                    description: |
                      Vérifiez que toutes les dépendances open source utilisées dans vos projets ne contiennent aucune vulnérabilité divulguée, qu'elles proviennent d'une source fiable et qu'elles n'ont pas été modifiées.

                      *  Générez une [nomenclature logicielle](https://docs.gitlab.com/ee/user/application_security/dependency_list/) de manière automatisée pour identifier les dépendances de vos projets
                      *  Identifiez automatiquement les vulnérabilités de tout logiciel dépendant utilisé au moyen d'une [analyse de la composition du logiciel](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) automatisée
                      *  Exécutez des analyses de [conformité des licences](https://docs.gitlab.com/ee/user/compliance/license_compliance/) pour vous assurer que votre projet utilise des logiciels dont les licences sont conformes aux politiques de votre entreprise
                  - title: Environnements de compilation sécurisés
                    description: |
                      Empêchez les acteurs malveillants d'injecter des codes malveillants dans le processus de compilation et de prendre le contrôle du logiciel créé par le pipeline ou d'accéder aux secrets utilisés dans le pipeline.

                      *  [Isolez votre environnement de compilation](https://docs.gitlab.com/runner/security/?_gl=1*1d95r9z*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MDA2LjAuMC4w) pour empêcher l'accès non autorisé ou l'exécution de codes malveillants
                      *  Conservez [les preuves de la release](https://docs.gitlab.com/ee/user/project/releases/#release-evidence) pour prouver tout ce qui y est inclus
                      *  Assurez-vous que vos artefacts de compilation ne sont pas compromis à l'aide d'une [attestation d'artefact de compilation](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-attestation)
                  - title: Artefacts de release sécurisés
                    description: |
                      Empêchez les attaquants d'exploiter les faiblesses de la conception ou de la configuration d'une application pour voler des données privées, obtenir un accès non autorisé à des comptes ou usurper l'identité d'utilisateurs légitimes.

                      *  Établissez une [connexion sécurisée](https://about.gitlab.com/blog/2022/01/07/gitops-with-gitlab-using-ci-cd/#meet-the-cicd-tunnel) avec votre cluster pour livrer vos artefacts de mise à jour
                      *  Identifiez [les vulnérabilités de sécurité dans les applications en cours d'exécution](https://docs.gitlab.com/ee/user/application_security/dast/) avant le déploiement
                      *  Assurez-vous que vos [interfaces API](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/) n'exposent pas votre application en cours d'exécution
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  Plateforme approuvée par les entreprises.
                  <br />
                  Adorée par les développeurs.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: Logo de Bending and Adelaide Bank
                    quote: Nous disposons désormais d'une solution toujours innovante qui s'aligne sur notre objectif de transformation numérique.
                    author: Caio Trevisan, Head of Devops Enablement, Bendigo and Adelaide Bank
                    ga_carousel: bending and adelaide bank testimonial
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: Logo New10
                    quote: GitLab nous aide vraiment dans notre architecture très moderne, parce que vous prenez en charge Kubernetes, vous prenez en charge serverless, et vous prenez en charge des fonctionnalités de sécurité sympas, comme DAST et SAST. GitLab nous permet d'avoir une architecture de pointe.
                    author: Kirill Kolyaskin, Directeur technique
                    ga_carousel: New10 testimonial
                    url: /customers/new10/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: Logo The Zebra
                    quote: La plus grande valeur (de GitLab) est que cette plateforme permet à nos équipes de développement de jouer un rôle plus important dans le processus de déploiement. Auparavant, seules quelques personnes connaissaient vraiment le processus, mais maintenant, la quasi-totalité de l'organisation de développement sait comment fonctionne le pipeline CI, peut travailler avec, y ajouter de nouveaux services, et mettre les choses en production sans que l'infrastructure ne soit le goulet d'étranglement.
                    author: Dan Bereczki,  Sr. Software Manager, The Zebra
                    url: /customers/thezebra/
                    ga_carousel: the zebra testimonial
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Logo Hilti
                    quote: La solution GitLab est empaquetés comme une suite et livrés avec un installateur très sophistiqué. Et pourtant tout fonctionne. C'est très pratique si vous êtes une entreprise qui souhaite simplement la mettre en place et la rendre opérationnelle.
                    author: Daniel Widerin, Head of Software Delivery, HILTI
                    url: /customers/hilti/
                    ga_carousel: hilti testimonial
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Quelle édition vous convient le mieux ?
                cta:
                  url: /pricing/
                  text: En savoir plus sur la tarification
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: 価格
                tiers:
                  - id: free
                    title: Free
                    items:
                      - Tests statiques de sécurité des applications (SAST) et détection des secrets
                      - Résultats dans le fichier json
                    link:
                      href: /pricing/
                      text: Lancez-vous
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: free tier
                  - id: premium
                    title: Premium
                    items:
                      - Tests statiques de sécurité des applications (SAST) et détection des secrets
                      - Résultats dans le fichier json
                      - Approbations MR et contrôles plus courants
                    link:
                      href: /pricing/
                      text: En savoir plus
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Tout ce qui compose l'édition Premium, plus
                      - Les scanners de sécurité complets incluent SAST, DAST, Secrets, dépendances, conteneurs, IaC, API, images de clusters et tests à données aléatoires
                      - Résultats exploitables dans le pipeline MR
                      - Pipelines de conformité
                      - Tableaux de bord de sécurité et de conformité
                      - Et bien plus encore
                    link:
                      href: /pricing/
                      text: Essayer Ultimate gratuitement
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: En savoir plus
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Ressources connexes
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: Contrôle en amont de la sécurité - présentation de DevSecOps
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: "https://www.youtube.com/embed/XnYstHObqlA"
                  data_ga_name: Shifting Security Left - DevSecOps Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: Gestion des vulnérabilités et séparation des tâches avec GitLab
                  link_text: "Regarder maintenant"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: "Release GitLab 15 : de nouvelles fonctionnalités de sécurité"
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: SBOM et attestation
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Icône d'un e-book
                  event_type: "Livre"
                  header: "Guide sur la sécurité de la chaîne d'approvisionnement logicielle"
                  link_text: "En savoir plus"
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Icône d'un e-book
                  event_type: "Livre"
                  header: "Enquête GitLab DevSecOps"
                  link_text: "En savoir plus"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: " GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "Guide absolu sur la sécurité de la chaîne d'approvisionnement logicielle "
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2022/08/30/the-ultimate-guide-to-software-supply-chain-security/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "Ultimate guide to software supply chain security "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "Se conformer au cadre NIST avec GitLab "
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/
                  image: "/nuxt-images/resources/resources_7.jpeg"
                  data_ga_name: "Comply to NIST framework with GitLab "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "Stratégies d'équipe de haut niveau pour sécuriser la chaîne d'approvisionnement logicielle "
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2022/01/06/elite-team-strategies-to-secure-software-supply-chains/
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  data_ga_name: "Elite team strategies to secure the software supply chain "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "Sécuriser la chaîne d'approvisionnement logicielle grâce à l'attestation automatisée"
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: "/nuxt-images/resources/resources_4.jpeg"
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Report Icon
                    variant: marketing
                  event_type: "Rapport d'analyste"
                  header: "GitLab obtient la position de Challenger dans le rapport Gartner Magic Quadrant 2022"
                  link_text: "En savoir plus"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Faites plus avec GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explorer d'autres solutions
          data_ga_name: solutions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab permet à vos équipes de concilier rapidité et sécurité en automatisant la livraison de logiciels et en sécurisant votre chaîne d'approvisionnement logicielle de bout en bout.
            icon:
              name: devsecops
              alt: devsecops Icon
              variant: marketing
            href: /solutions/security-compliance/
            cta: En savoir plus
            data_ga_name: devsecops learn more
            data_ga_location: body
          - title: Conformité continue du logiciel
            description: Il est facile d'intégrer la sécurité dans votre cycle de vie DevSecOps avec GitLab.
            icon:
              name: build
              alt: Icône de compilation
              variant: marketing
            href: /solutions/continuous-software-compliance/
            cta: En savoir plus
            data_ga_name: continuous software compliance learn more
            data_ga_location: body
          - title: Intégration et livraison continues
            description: Mettez en place une livraison de logiciels reproductible et à la demande
            icon:
              name: continuous-delivery
              alt: Livraison continue
              variant: marketing
            href: /solutions/continuous-integration/
            cta: En savoir plus
            data_ga_name: siemens learn more
            data_ga_location: body

