---
  title: Intégrez Kubernetes à votre cycle de vie DevOps
  description: L'intégration Kubernetes de GitLab vous permet de créer, de tester, de déployer et d'exécuter votre application à grande échelle.
  components:
    - name: 'solutions-hero'
      data:
        title: Kubernetes + GitLab
        subtitle: Tout ce qu'il vous faut pour créer, tester, déployer et exécuter votre application à grande échelle
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /webcast/scalable-app-deploy/
          text: Découvrez comment GitLab peut aider votre équipe à déployer des applications à grande échelle !
          data_ga_name: scale app deployment
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image\_: GitLab + Kubernetes"
    - name: copy-media
      data:
        block:
          - header: La meilleure solution pour le développement cloud-native
            miscellaneous: |
              Les applications cloud-native sont l'avenir du développement de logiciels. Les systèmes cloud native, conditionnés en conteneurs, gérés dynamiquement et axés sur les microservices, permettent un développement plus rapide tout en maintenant la stabilité opérationnelle.

              GitLab est une application unique comportant tout ce qu'il vous faut pour [le développement et les opérations logicielles de bout en bout](/stages-devops-lifecycle/){data-ga-name="devops lifecycle" data-ga-location="body"}. Du suivi des tickets à la gestion du code source en passant par CI/CD et la surveillance : avoir tout en un seul endroit simplifie la complexité de la chaîne d'outils et accélère les durées de cycle. Avec un [registre de conteneurs intégré](https://docs.gitlab.com/ee/user/packages/container_registry/index.html){data-ga-name="container registry" data-ga-location="body"} et une [intégration Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"}, GitLab facilite plus que jamais le lancement de la conteneurisation et du développement cloud-native, ainsi que l'optimisation de vos processus de développement d'applications dans le Cloud.
          - header: Qu'est-ce que Kubernetes ?
            miscellaneous: |
              Kubernetes est une plateforme d'orchestration de conteneurs open source. Elle est conçue pour automatiser votre gestion des conteneurs d'applications, du déploiement à l'exploitation en passant par la mise à l'échelle. L'orchestration de Kubernetes vous permet de partitionner au fur et à mesure de la mise à l'échelle selon les besoins. Vous pouvez ainsi répondre rapidement et efficacement à la demande de la clientèle tout en limitant l'utilisation du matériel dans votre environnement de production et en minimisant les perturbations des déploiements de fonctionnalités.
            media_link_href: /blog/2017/11/30/containers-kubernetes-basics/
            media_link_text: En savoir plus sur Kubernetes
            media_link_data_ga_name: more about kubernetes
            media_link_data_ga_location: body
          - header: Déployez GitLab sur Kubernetes ou utilisez GitLab pour tester et déployer votre logiciel sur Kubernetes
            miscellaneous: |
              GitLab fonctionne avec ou au sein de Kubernetes de trois manières distinctes. Celles-ci peuvent être utilisées ensemble ou séparément.

              * [Déployer les logiciels de GitLab dans Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="deploy with kubernetes" data-ga-location="body"}
              * [Utiliser Kubernetes pour gérer les runners attachés à votre instance GitLab](https://docs.gitlab.com/runner/install/kubernetes.html){data-ga-name="kubernetes runners" data-ga-location="body"}
              * [Exécuter l'application et les services GitLab sur un cluster Kubernetes](https://docs.gitlab.com/charts/){data-ga-name="kubernetes cluster" data-ga-location="body"}

              Chaque approche décrite ci-dessus peut être utilisée avec ou sans les autres. Par exemple, une instance GitLab Omnibus s'exécutant sur une machine virtuelle peut déployer un logiciel stocké dans celle-ci sur Kubernetes via un runner Docker.
          - header: Intégration Kubernetes
            inverted: true
            text: |
              Même si vous pouvez utiliser GitLab pour déployer vos applications presque partout, du bare metal aux VM, GitLab est conçu pour Kubernetes. [L'intégration Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"} vous donne accès à des fonctionnalités avancées telles que :

              * [Déploiements sur base d'un système Pull](https://docs.gitlab.com/ee/user/clusters/agent/repository.html#synchronize-manifest-projects){data-ga-name="pull-based deployments" data-ga-location="body"}
              * [Déploiement depuis GitLab CI/CD via une connection sécurisée](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html){data-ga-name="CI/CD secure connection" data-ga-location="body"}
              * [Déploiements canari](https://docs.gitlab.com/ee/user/project/canary_deployments.html){data-ga-name="canary deployments" data-ga-location="body"}
              * [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/){data-ga-name="auto devops" data-ga-location="body"}
            icon:
              use_local_icon: true
              name: /nuxt-images/logos/kubernetes.svg
              alt: Kubernetes
          - header: Utilisez GitLab pour tester et déployer votre application sur Kubernetes
            text: |
              [GitLab CI/CD](/features/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"} vous permet de gérer facilement les déploiements dans plusieurs environnements. Exécutez des tests automatisés en parallèle à la mise à l'échelle automatique [des runners GitLab](https://docs.gitlab.com/runner/){data-ga-name="gitlab runners" data-ga-location="body"}. Testez les modifications manuellement dans un environnement de production avant de fusionner le code avec les applications de revue de code. Les runners, les [applications de revue de code](/stages-devops-lifecycle/review-apps/){data-ga-name="review apps" data-ga-location="body"} et votre propre application peuvent être déployés sur votre cluster Kubernetes ou tout autre environnement de votre choix.
            icon:
              name: rocket-alt
              alt: Fusée
              variant: marketing
              hex_color: '#F43012'
            link_href: /partners/technology-partners/google-cloud-platform/
            link_text: Déployer sur la plateforme Google Cloud
            link_data_ga_name: deploy on GCP
            link_data_ga_location: body
    - name: featured-media
      data:
        column_size: 3
        horizontal_rule: true
        media:
          - title: Documentation Auto DevOps
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-devops.png
              alt: "DevOps"
            link:
              text: En savoir plus
              href: https://docs.gitlab.com/ee/topics/autodevops/index.html
              data_ga_name: DevOps documentation
              data_ga_location: body
          - title: Créer un pipeline CI/CD avec déploiement automatique
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm.png
              alt: "Article de blog Créer un pipeline CI/CD"
            link:
              text: En savoir plus
              href: /blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
              data_ga_name: CI/CD with auto deploy
              data_ga_location: body
          - title: Installer GitLab sur Kubernetes
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-gitlab-kubernetes.png
              alt: "Article de blog Créer un pipeline CI/CD"
            link:
              text: En savoir plus
              href: https://docs.gitlab.com/charts/
              data_ga_name: install gitlab on kubernetes
              data_ga_location: body
          - title: Webinaire Cloud Native
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-webcast.png
              alt: "Webdiffusion GitLab"
            link:
              text: Regarder maintenant
              href: /blog/2017/04/18/cloud-native-demo/
              data_ga_name: cloud native webinar
              data_ga_location: body
