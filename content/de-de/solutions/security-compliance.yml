---
  title: Integriere Sicherheit in deinen DevOps-Lebenszyklus
  description: GitLab-Anwendungssicherheitstests für SAST, DAST, Abhängigkeitssuche, Container-Scans und mehr innerhalb der DevSecOps CI-Pipeline mit Sicherheitslückenmanagement und Compliance.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  Linksverschiebung von Sicherheit und Konformität
        title: GitLab-Sicherheit und -Governance
        subtitle: GitLab versetzt deine Teams in die Lage, Geschwindigkeit und Sicherheit in Einklang zu bringen, indem es die Softwarebereitstellung automatisiert und deine Softwarebereitstellungskette durchgängig schützt.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimate kostenlos testen
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Noch Fragen? Frag uns!
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          image_url_mobile: /nuxt-images/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          alt: "Bild: GitLab für den öffentlichen Sektor"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Vertraut von
        logos:
          - name: UBS Logo
            image: "/nuxt-images/home/logo_ubs_mono.svg"
            url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: Link zur UBS Kunden-Fallstudie
          - name: HackerOne Logo
            image: /nuxt-images/logos/hackerone-logo.png
            url: https://about.gitlab.com/customers/hackerone/
            aria_label: Link zur HackerOne Kunden-Fallstudie
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: https://about.gitlab.com/customers/thezebra/
            aria_label: Link zur The Zebra Kunden-Fallstudie
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: https://about.gitlab.com/customers/hilti/
            aria_label: Link zur Hilti Kunden-Fallstudie
          - name: Conversica Logo
            image: /nuxt-images/logos/conversica.svg
            url: https://about.gitlab.com/customers/conversica/
            aria_label: Link zur Kunden-Fallstudie con Conversica
          - name: Bendigo and Adelaide Bank Logo
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: https://about.gitlab.com/customers/bab/
            aria_label: Link zur Kunden-Fallstudie der Bendigo and Adelaide Bank
          - name: Glympse Logo
            image: /nuxt-images/logos/glympse-logo-mono.svg
            url: https://about.gitlab.com/customers/glympse/
            aria_label: Link zur Glympse Kunden-Fallstudie
    - name: 'side-navigation-variant'
      links:
        - title: Übersicht
          href: '#overview'
        - title: Vorteile
          href: '#benefits'
        - title: Leistungen
          href: '#capabilities'
        - title: Kundenstimmen
          href: '#customers'
        - title: Preise
          href: '#pricing'
        - title: Ressourcen
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Schnell und sicher bereitstellen
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: kontinuierliche Integration
                      variant: marketing
                    header: Integrierte Sicherheit
                    text: Eine Plattform, ein Preis, alles sofort einsatzbereit.
                    link_text: Erfahre mehr über die DevSecOps-Plattform
                    link_url: /platform
                    ga_name: platform
                    ga_location: overview
                  - icon:
                      name: devsecops
                      alt: DevSecOps
                      variant: marketing
                    header: Kontinuierliche Sicherheit
                    text: Automatisierte Scans vor und nach dem Pushen von Code.
                    link_text: Gründe, die für GitLab sprechen
                    link_url: /why-gitlab/
                    ga_name: why gitlab
                    ga_location: overview
                  - icon:
                      name: release
                      alt: Schutzschild mit Häkchen
                      variant: marketing
                    header: Umfassende Kontrolle
                    text: Implementiere Schutzmaßnahmen und automatisiere Richtlinien.
                    link_text: Erfahre mehr über den Ansatz unserer Plattform
                    ga_name: devops platform
                    ga_location: overview
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              data:
                video:
                  url: 'https://player.vimeo.com/video/762685637?h=f96e969756'
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Sicherheit. Konformität. Nach links verschoben.
                cards:
                  - title: Schütze deine Softwarebereitstellungskette
                    description: GitLab unterstützt dich dabei, deine Softwarebereitstellungskette (einschließlich Quelle, Build, Abhängigkeiten und freigegebene Artefakte) zu schützen, eine Bestandsaufnahme der verwendeten Software (Software-Leistungsbeschreibung) zu erstellen und die erforderlichen Kontrollen anzuwenden.
                    data_ga_name: supply chain
                    data_ga_location: benefits
                    cta: Erfahren Sie mehr
                    icon:
                      name: less-risk
                      alt: Geringeres Risiko
                      variant: marketing
                    href: /solutions/supply-chain/
                  - title: Verwaltung von Bedrohungsvektoren
                    description: GitLab hilft dir bei der Linksverschiebung der Sicherheit, indem es Quellcode, Container, Abhängigkeiten und ausgeführte Anwendungen automatisch auf Schwachstellen scannt. Es können Sicherheitskontrollen eingerichtet werden, um deine Produktivumgebung zu schützen.
                    data_ga_name: docs app security
                    data_ga_location: benefits
                    cta: Erfahren Sie mehr
                    icon:
                      name: devsecops
                      alt: DevSecOps
                      variant: marketing
                    href: https://docs.gitlab.com/ee/user/application_security/get-started-security.html
                  - title: Einhaltung von Konformitätsanforderungen
                    description: GitLab kann dich dabei unterstützen, deine Änderungen zu verfolgen, die notwendigen Kontrollen zu implementieren, um abzusichern, was in die Produktion einfließt, und die Einhaltung der Lizenzkonformität sowie der regulatorischen Framework-Bedingungen sicherzustellen.
                    data_ga_name: compliance
                    data_ga_location: benefits
                    cta: Erfahren Sie mehr
                    icon:
                      name: eye-magnifying-glass
                      alt: Auge in Lupe
                      variant: marketing
                    href: /solutions/compliance/
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Linksverschieben der Sicherheit
                sub_description: ''
                white_bg: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                solutions:
                  - title: Integration von Sicherheitstests in die CI/CD-Pipeline
                    description: Verwende unsere integrierten Scanner und integriere benutzerdefinierte Scanner. Verschiebe die Sicherheit nach links, um Entwickler(innen) in die Lage zu versetzen, Sicherheitslücken zu finden und zu beheben, sobald sie auftreten. Zu den umfassenden Scannern gehören SAST, DAST, Geheimnissuche, Abhängigkeitssuche, Containerscans, IaC-Scans, API-Sicherheit sowie Fuzz-Tests.
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/?_gl=1*hwzvlj*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2MzIyOTY1My44OS4xLjE2NjMyMzAyNTYuMC4wLjA.
                    data_ga_name: ci/cd
                    data_ga_location: solutions block
                  - title: Verwalten von Abhängigkeiten
                    description: Angesichts der Vielzahl von Open-Source-Komponenten, die heute in der Softwareentwicklung verwendet werden, ist die manuelle Verwaltung dieser Abhängigkeiten eine herausfordernde Aufgabe. Scanne Anwendungs- und Containerabhängigkeiten auf Sicherheitslücken und erstelle eine Software-Leistungsbeschreibung (SBOM) der verwendeten Abhängigkeiten.
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: dependencies management
                    data_ga_location: solutions block
                  - title: Verwaltung von Sicherheitslücken
                    description: Skaliere Sicherheitsteams, indem du Schwachstellen im natürlichen Workflow von Entwickler(inne)n aufdeckst und behebst, bevor Code in produktive Branches gepuscht wird. Sicherheitsexperten können Sicherheitslücken von Pipelines, On-Demand-Scans, Drittanbietern und Bug-Bounties an einem Ort überprüfen, einstufen und verwalten.
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: Schutz für laufende Anwendungen
                    description: Schütze deine Workloads, indem du einen sicheren CI/CD-Tunnel mit deinen Clustern einrichtest, dynamische Anwendungssicherheitsscans sowie Container-Scans durchführst und IP-Whitelisting einrichtest.
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: secure applications
                    data_ga_location: solutions block
                  - title: Implementiere Schutzmaßnahmen und stelle die Konformität sicher
                    description: Automatisiere Sicherheits- und Konformitätsrichtlinien im gesamten Lebenszyklus deiner Softwareentwicklung. Konforme Pipelines stellen sicher, dass Pipeline-Richtlinien nicht umgangen werden, während gängige Kontrollen End-to-End-Sicherheitsmaßnahmen bereitstellen.
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: guardrails guardrails
                    data_ga_location: solutions block
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  Mit dem Vertrauen von Unternehmen.
                  <br />
                  Von Entwickler(inne)n geliebt.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/hackerone-logo.png
                      alt: Hackerone Logo
                    quote: GitLab hilft uns dabei, Sicherheitslücken frühzeitig zu erkennen. Es ist inzwischen fest in die Arbeitsabläufe der Entwickler(innen) integriert. Entwickler(innen) können Code in GitLab CI pushen und eine sofortige Rückmeldung von einem der vielen kaskadierenden Audit-Schritte erhalten. So können sie sofort sehen, ob es eine Sicherheitslücke gibt. Zusätzlich können Entwickler(innen) eigene neue Schritte erstellen, die ganz bestimmte Sicherheitsprobleme testen.
                    author: Mitch Trale
                    position: Head of Infrastrusture, HackerOne
                    ga_carousel: hackerone testimonial
                    ga_location: customers
                    url: /customers/hackerone/
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: Bendigo and Adelaide Bank Logo
                    quote: Wir haben jetzt eine Lösung, die stets innovativ ist und zu unserem Ziel der digitalen Transformation passt.
                    author: Caio Trevisan
                    position: Head of Devops Enablement, Bendigo and Adelaide Bank
                    ga_carousel: bendigo and adelaide bank testimonial
                    ga_location: customers
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: The Zebra Logo
                    quote: Der größte Vorteil (von GitLab) ist, dass es den Entwicklungsteams ermöglicht, eine größere Rolle innerhalb des Bereitstellungsprozesses zu spielen. Früher kannten nur wenige Leute sich wirklich gut aus. Aber heute weiß so ziemlich die gesamte Entwicklungsabteilung, wie die CI-Pipeline funktioniert, und alle können damit arbeiten, neue Services hinzufügen und Neues in die Produktion einbringen, ohne dass die Infrastruktur einen Engpass darstellt.
                    author: Dan Bereczki
                    position: Sr. Software Manager, The Zebra
                    ga_carousel: the zebra testimonial
                    ga_location: customers
                    url: /customers/thezebra/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hilti Logo
                    quote: GitLab ist wie eine Suite gebündelt und wird mit einem sehr ausgeklügelten Installationsprogramm ausgeliefert. Und dann funktioniert es wie von Zauberhand. Das ist sehr schön, wenn man ein Unternehmen ist, das einfach nur alles zum Laufen bringen möchte.
                    author: Daniel Widerin
                    position: Head of Software Delivery, HILTI
                    ga_location: customers
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Welche Stufe ist die richtige für Sie?
                cta:
                  url: /pricing/
                  text: Erfahre mehr über die Preise
                  data_ga_name: pricing learn more
                  data_ga_location: pricing
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Free
                    items:
                      - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
                      - Ergebnisse in JSON-Datei
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: free learn more
                      data_ga_location: pricing
                      aria_label: free tier
                  - id: premium
                    title: Premium
                    items:
                      - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
                      - Ergebnisse in JSON-Datei
                      - MR-Genehmigungen und häufigere Kontrollen
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: premium learn more
                      data_ga_location: pricing
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Alle Funktionen von Premium und zusätzlich
                      - Umfassende Sicherheitsscanner umfassen SAST, DAST, Geheimnisse, Abhängigkeiten, Container, IaC, APIs, Cluster-Images und Fuzz-Tests
                      - Umsetzbare Ergebnisse innerhalb der MR-Pipeline
                      - Konformitäts-Pipelines
                      - Sicherheits- und Konformitäts-Dashboards
                      - Vieles mehr
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: ultimate learn more
                      data_ga_location: pricing
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: Ultimate kostenlos testen
                      data_ga_name: try ultimate
                      data_ga_location: pricing
                      aria_label: ultimate tier
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Zugehörige Ressourcen
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Video
                  event_type: "Video"
                  header: Verlagerung der Sicherheit nach links – GitLab-Sicherheitsübersicht
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: https://www.youtube.com/embed/XnYstHObqlA?enablejsapi=1
                  data_ga_name: Shifting Security Left - GitLab Security Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video
                  event_type: "Video"
                  header: Schwachstellen verwalten und Aufgabentrennung ermöglichen mit GitLab
                  link_text: "Jetzt ansehen"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI?enablejsapi=1
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video
                  event_type: "Video"
                  header: GitLab 15 Release – Neue Sicherheitsfunktionen
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo?enablejsapi=1
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video
                  event_type: "Video"
                  header: SBOM and Attestation
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0?enablejsapi=1
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: E-Book
                  event_type: "Book"
                  header: "Leitfaden zur Sicherheit der Software-Lieferkette"
                  link_text: "Mehr erfahren"
                  image: "/nuxt-images/resources/resources_11.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: E-Book
                  event_type: "Book"
                  header: "DevSecOps mit GitLab CI/CD"
                  link_text: "Mehr erfahren"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://page.gitlab.com/achieve-devsecops-cicd-ebook.html
                  data_ga_name: "DevSecOps with GitLab CI/CD"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: E-Book
                  event_type: "Book"
                  header: "GitLab DevSecOps-Umfrage"
                  link_text: "Mehr erfahren"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog
                  event_type: "Blog"
                  header: "9 Tipps zum Verschieben nach links"
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2020/06/23/efficient-devsecops-nine-tips-shift-left/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "9 tips to shift left"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog
                  event_type: "Blog"
                  header: "Sicherung der Software-Lieferkette durch automatisierte Attestierung"
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Bericht
                    variant: marketing
                  event_type: "Analystenbericht"
                  header: "451 Forschungsmeinung: GitLab erweitert den Blick auf DevOps"
                  link_text: "Mehr erfahren"
                  href: "https://page.gitlab.com/resources-report-451-gitlab-broadens-devops.html"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "451 Research opinion: GitLab broadens view of DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Bericht
                    variant: marketing
                  event_type: "Analystenbericht"
                  header: "GitLab ist ein Herausforderer im Gartner Magic Quadrant 2022"
                  link_text: "Mehr erfahren"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/fjct_cover.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: 'report-cta'
      data:
        layout: "dark-shortened"
        title: Analystenberichte
        reports:
        - description: "GitLab als einziger Marktführer in The Forrester Wave™ anerkannt: Integrierte Software-Entwicklungsplattformen, Q2 2023"
          url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
          link_text: Lesen Sie den Bericht
        - description: "GitLab als Marktführer im Gartner® Magic Quadrant 2023™ für DevOps-Plattformen anerkannt"
          url: /gartner-magic-quadrant/
          link_text: Lesen Sie den Bericht
    - name: solutions-cards
      data:
        title: Machen Sie mehr mit GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Erkunde weitere Lösungen
          data_ga_name: all solutions
          data_ga_location: do more with gitlab
        cards:
          - title: Kontinuierliche Software-Konformität
            description: Integriere Sicherheit in deinen DevOps-Lebenszyklus – mit GitLab ist das ganz einfach.
            icon:
              name: continuous-integration
              alt: kontinuierliche Integration
              variant: marketing
            href: /solutions/compliance/
            data_ga_name: compliance
            data_ga_location: do more with gitlab
            cta: Erfahren Sie mehr
          - title: Sicherheit der Softwarebereitstellungskette
            description: Stelle sicher, dass deine Softwarebereitstellungskette sicher und konform ist.
            icon:
              name: devsecops
              alt: DevSecOps
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: supply chain
            data_ga_location: do more with gitlab
            cta: Erfahren Sie mehr
          - title: Kontinuierliche Integration und Bereitstellung
            description: Wiederholbare und bedarfsgerechte Softwarebereitstellung
            icon:
              name: continuous-delivery
              alt: Kontinuierliche Bereitstellung
              variant: marketing
            href: /solutions/continuous-integration/
            data_ga_name: ci
            data_ga_location: do more with gitlab
            cta: Erfahren Sie mehr

