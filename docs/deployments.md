# Deployments

## Build step

Since this site is meant to be a subset of pages built for about.gitlab.com, with the rest of it handled in [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com), we disable the [crawler attribute of the generator](https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-generate#crawler). Missing relative links are assumed to be created elsewhere.

## GitLab CI

Refer to the `.gitlab-ci.yml` file for up to date documentation on the pipeline. Since pipelines can grow in scope and get complex to follow, prefer documenting the `.gitlab-ci.yml` file with inline comments over updating documentation in this markdown file. 

## Deploy script

Deployments are handled through a shell script in `scripts/deploy`. As with the `.gitlab-ci.yml` file, prefer documenting decisions in that file directly rather than in this file. At a high level, the deploy script should be able to be called from the CI pipeline and take contextual deployment actions based on variables made available in the CI environment it has been called from. That way we can manage the CI complexity in one place (`.gitlab-ci.yml`), and manage deployment complexity mostly in one place (`scripts/deploy`).

## Contentful

Our pipeline's build step includes a multitude of API calls to Contentful. Because our site is statically generated, each page or template is making an API call for content as well as functions being fired to create dynamic routes based off of pages created in Contentful.

### Dynamic routes

Nuxt does not know which dynamic routes to generate now that our pages are created in Contentful. We explicitly tell Nuxt which pages to generate based off varying criteria from `slug` name to `Content Type` name.

Please become familiar with our [route generation](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/blob/main/route.contentful.js?ref_type=heads) functions to learn more about how certain child pages get generated.

### Draft vs Published

By default, the build step in our Review App and Production pipelines will grab all **published** content. If you have a merge request that depends on draft content, add the following snippet to your Merge Request's description. You may have to re-run the pipeline.

```
**Build Variables:**
- [x] Use Contentful Preview API
```

### Webhooks

Currently, we have a Contentful webhook that gets triggered any time an Entry has been published or unpublished. This triggers a pipeline to re-run using the latest Contentful data.


