# Content

Our content has been primiarily stored in [Contentful](https://www.contentful.com/) since late 2023. Historically, we have used the [Nuxt content module](https://content.nuxtjs.org/) and utilized yml files to store content in data schemas. At the time of this documentation update, we still have some content living on our `/content` folder but we are actively migrating this over. 

For more information on how to update content in Contentful, see our dedicated [handbook](https://handbook.gitlab.com/handbook/marketing/digital-experience/contentful-cms/) section.

At GitLab, we want everyone to contribute. We want to encourage GitLab team members to contribute to content updates across our marketing site. If a team member reaches out to Digital Experience about a content update, we can direct them towards our handbook documentation so they can get up and running with our CMS, allowing them to self-serve.

## Localization efforts

Our marketing site currently draws from English, French, German, and Japanese sources. English is the default language used, with new pages being built with English in mind. Our site is progressively being translated. Translated content currently lives in our `/content/` folder and Contentful with the goal to be fully migrated to the latter. 

Questions specific to translated content should be relayed to our [Localization](https://handbook.gitlab.com/handbook/marketing/localization/) team.

## Developing around content

### I18n module

Because we have localized content, we use the [Nuxt i18n](https://i18n.nuxtjs.org/) module on most pages. Pages that are not drawing from localized content will have a `nuxtI18n: false` property. 

### Content module

For remaining pages that are still using the Content module, the asyncData function should look similar to:

```
async asyncData({ $content }: any) {
    try {
      const pageData = await $content(`folder-name/file-name`).fetch();
      return { pageData };
    } catch (e) {
      throw new Error(e);
    }
  },
```

### Contentful Draft vs Published 

Content in Contentful, our CMS, can be in draft mode or can be published. Published content is used in production. There will most likely be instances where you are creating a new feature that draws from draft content, or content that should not be published yet. 

When developing locally, the site will be drawing from published data by default. You can adjust your environment variables to work with draft and published content.

Assuming you have the appropriate Contentful access tokens, you should add `USE_CONTENTFUL_PREVIEW_API="true"` to your `.env` file.


### Assets

We have historically hosted site assets (images and PDFs) within the project. Going forward, please host these types of assets in Contentful.

Videos are handled by our Video and Brand teams. Our company practice is to use Vimeo-hosted videos as much as possible. (See our `embedded-videos.md` doc for more information)

Icons are hosted in our Storybook instance of our design system, Slippers.