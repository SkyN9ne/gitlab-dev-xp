import Vue from 'vue';
import { kebabCase } from 'lodash';

export const ContentfulComponentMixin = Vue.extend({
  computed: {
    headerId() {
      return this.data?.headerAnchorId
        ? this.data?.headerAnchorId
        : kebabCase(this.data?.header);
    },
  },
});
