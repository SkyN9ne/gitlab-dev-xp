import Vue from 'vue';

export const editQueryParams = Vue.extend({
  methods: {
    updateFreeTrialGlmContent(currentString: string) {
      let queryString;
      const split: string[] = currentString.split('?');

      if (split.length === 1) {
        queryString = '';
      } else {
        queryString = split[split.length - 1];
      }

      const searchParams = new URLSearchParams(queryString);

      if (
        !searchParams.has('glm_source') ||
        searchParams.get('glm_source') !== 'about.gitlab.com'
      ) {
        return currentString;
      }

      if (this.$route.path === '/') {
        searchParams.set('glm_content', 'home-page');
      } else {
        searchParams.set(
          'glm_content',
          this.$route.path.replace(/^\/|\/$/g, ''), // remove leading and trailing slash
        );
      }

      const updatedQueryString = `${split[0]}?${decodeURIComponent(searchParams.toString())}`;

      return updatedQueryString;
    },
  },
});
