import { CtfCard, CtfEntry, CtfNextStep } from '~/models';

export function nextStepsAltHelper(data: CtfNextStep) {
  const ctaGroup = data.blockGroup.map((block: CtfEntry<CtfCard>) => {
    return {
      icon: block.fields.iconName && {
        name: `slp-${block.fields.iconName}`,
        variant: 'marketing',
        alt: block.fields.iconName,
        size: 'md',
      },
      blurb: block.fields.title,
      button: block.fields.button && {
        text: block.fields.button.fields.text,
        href: block.fields.button.fields.externalUrl,
        data_ga_name: block.fields.button.fields.dataGaName,
        data_ga_location: block.fields.button.fields.dataGaLocation,
      },
    };
  });
  const transformedData = {
    header: data.heading,
    blurb: data.tagline,
    button: {
      text: data.primaryButton.fields.text,
      href: data.primaryButton.fields.externalUrl,
      data_ga_name: data.primaryButton.fields.dataGaName,
      data_ga_location: data.primaryButton.fields.dataGaLocation,
    },
    cta_group: ctaGroup,
  };
  return transformedData;
}
