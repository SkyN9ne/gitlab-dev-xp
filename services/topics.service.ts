import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { toKebabCase } from '@/lib/utils';
import { CtfEntry, CtfTopic, CtfTopicCard } from '~/models';
import { LOCALES } from '~/common/constants';
import { convertToCaseSensitiveLocale } from '~/common/util';

export async function fetchTopicBySlug(
  slug: string,
  locale: string = LOCALES.DEFAULT,
) {
  const completeSlug = `/topics/${slug}/`;

  const client = getClient();
  const entries = await client.getEntries({
    'fields.slug': completeSlug,
    content_type: CONTENT_TYPES.TOPICS,
    include: 2,
    locale: convertToCaseSensitiveLocale(locale),
  });

  if (entries.items.length > 0) {
    return entries.items[0].fields;
  }
  return null;
}

export function handleTopicFromContentful(topic: CtfTopic) {
  function generateBreadcrumbs() {
    const baseHref = '/';
    const crumbs = [];

    const parts = topic.slug.split('/').filter(Boolean);

    let currentPath = baseHref;

    parts.forEach((part, index) => {
      currentPath += `${part}/`;
      const title = part.replace(/-/g, ' ');
      const formattedTitle = title.charAt(0).toUpperCase() + title.slice(1);

      if (index !== parts.length - 1) {
        crumbs.push({
          title: formattedTitle,
          href: currentPath,
          data_ga_name: part,
          data_ga_location: 'breadcrumb',
        });
      }
    });

    crumbs.push({ title: topic?.title });

    return crumbs;
  }

  function mapResourceCards({ title, grouped, cards }) {
    return {
      name: 'solutions-resource-cards',
      data: {
        title,
        grouped,
        column_size: 4,
        cards: cards.map((entry: CtfEntry<CtfTopicCard>) => {
          const card = {
            icon: {
              name: entry.fields.iconName,
              variant: 'marketing',
              alt: '',
            },
            event_type: entry.fields.subtitle,
            header: entry.fields?.title,
            link_text: entry.fields?.button?.fields?.text,
            href: entry.fields?.button?.fields?.externalUrl,
            data_ga_name: entry.fields.title,
            data_ga_location: 'resource cards',
          };

          if (entry.fields.image?.fields?.file?.url) {
            card.image = `${entry.fields.image?.fields?.file?.url}?h=400&fl=progressive`;
          }
          return card;
        }),
      },
    };
  }

  try {
    const topLevelFields = {
      title: topic.seoTitle || topic.seo?.fields.ogTitle,
      description: topic.seoDescription || topic.seo?.fields.ogDescription,
      date_published: topic.datePublished,
      date_modified: topic.dateModified,
      topic_name: topic.topicName,
      icon: topic.icon,
      topics_breadcrumb: true,
    };

    const topicsHeader = {
      data: {
        title: topic.topicName,
        block: [
          {
            text: topic.headerText,
            link_text: topic.headerCta
              ? topic.headerCta.fields.text
              : undefined,
            link_href: topic.headerCta
              ? topic.headerCta.fields.externalUrl
              : undefined,
          },
        ],
      },
    };

    const crumbs = generateBreadcrumbs();

    const modifiedBodyContent = [];

    topic.bodyContent.forEach((entry, index) => {
      if (entry.fields && entry.fields.url) {
        if (index > 0) {
          modifiedBodyContent[modifiedBodyContent.length - 1].fields.video_url =
            entry.fields.url;
        }
      } else {
        modifiedBodyContent.push(entry);
      }
    });

    const sideMenu = {
      anchors: {
        text: topic.bodyContent.every((entry) => !entry.fields.header)
          ? ''
          : 'On this page',
        data: topic.bodyContent
          .filter((entry) => entry.fields && entry.fields.header)
          .map((entry) => {
            return {
              text: entry.fields.header,
              href: entry.fields.headerAnchorId
                ? entry.fields.headerAnchorId
                : `#${toKebabCase(entry.fields.header)}`,
              'data-ga-title': entry.fields.header.toLowerCase(),
              'data-ga-location': 'side-navigation',
            };
          }),
      },
      content: modifiedBodyContent
        .filter((entry) => entry.fields && entry.fields.text)
        .map((entry) => ({
          name: 'topics-copy-block',
          data: {
            header: entry.fields.header,
            column_size: 10,
            blocks: [
              {
                text: entry.fields.text,
                video_url: entry.fields.video_url
                  ? entry.fields.video_url
                  : undefined,
              },
            ],
          },
        })),
    };

    if (topic.sideMenuHyperlinks) {
      sideMenu.hyperlinks = {
        text: topic.sideMenuTitle,
        data: topic.sideMenuHyperlinks
          .filter((entry) => entry.fields && entry.fields.text)
          .map((entry) => {
            return {
              text: entry.fields.text,
              href: entry.fields.externalUrl,
              variant: entry.fields.variation,
              'data-ga-name': entry.fields.text.toLowerCase(),
              'data-ga-location': 'side-navigation',
            };
          }),
      };
    }

    if (topic.footerCta) {
      const component = {
        name: 'topics-cta',
        data: {
          title: topic.footerCta.fields?.title,
          subtitle: topic.footerCta.fields?.subtitle,
          text: topic.footerCta.fields?.description,
          column_size: 10,
        },
        cta_one: topic.footerCta.fields?.button && {
          text: topic.footerCta.fields.button.fields.text,
          link: topic.footerCta.fields.button.fields.externalUrl,
          data_ga_name: topic.footerCta.fields.button.fields.text,
          data_ga_location: 'body',
        },
        cta_two: topic.footerCta.fields?.secondaryButton && {
          text: topic.footerCta.fields.secondaryButton.fields.text,
          link: topic.footerCta.fields.secondaryButton.fields.externalUrl,
          data_ga_name: topic.footerCta.fields.secondaryButton.fields.text,
          data_ga_location: 'body',
        },
      };
      sideMenu.content.push(component);
    }

    const components = [];

    if (topic.groupedResourceTitle) {
      components.push(
        mapResourceCards({
          title: topic.groupedResourceTitle,
          grouped: true,
          cards: topic.groupedResourceCards,
        }),
      );
    }

    if (topic.resourceCardTitle) {
      components.push(
        mapResourceCards({
          title: topic.resourceCardTitle,
          grouped: false,
          cards: topic.resourceCards,
        }),
      );
    }

    const document = {
      ...topLevelFields,
      topicsHeader,
      crumbs,
      sideMenu,
      components,
    };

    return document;
  } catch (e) {
    return null;
  }
}
