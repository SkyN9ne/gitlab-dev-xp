export function demoDataHelper(data: any[]) {
  // Hero
  const heroData = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );

  // Content
  const content = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'headerAndText',
  );
  const pageData = {
    header: heroData[0].fields.title,
    subtitle: heroData[0].fields.description,
    content: content[0].fields.text,
    demo_video: {
      video_url: heroData[0].fields.video.fields.url,
      size: 'lg',
      header: null,
      aos_animation: 'fade-down',
      aos_duration: 500,
      all_fields_required: true,
    },
  };

  return pageData;
}
