import axios from 'axios';
const { DEX_SERVICE_READ_WRITE_TOKEN } = process.env;
const API_URL = 'https://gitlab.com/api/v4';
const PROJECT_ID = '28847821';
const MONTHS_SINCE_LAST_UPDATED = 9; // Close issues that have not been updated after this many months

const calculateDateMonthsAgo = (months) => {
  const date = new Date();
  date.setMonth(date.getMonth() - months);
  return date.toISOString();
};

async function closeInactiveIssues() {
  try {
    const updatedBeforeDate = calculateDateMonthsAgo(MONTHS_SINCE_LAST_UPDATED);
    const response = await axios.get(
      `${API_URL}/projects/${PROJECT_ID}/issues`,
      {
        headers: {
          'PRIVATE-TOKEN': DEX_SERVICE_READ_WRITE_TOKEN,
          'Content-Type': 'application/json',
        },
        params: {
          updated_before: updatedBeforeDate,
          state: 'opened',
        },
      },
    );

    if (response.status === 200) {
      const issues = response.data;

      for (const issue of issues) {
        const issueIid = issue.iid;
        const comment = `This issue has not been updated in ${MONTHS_SINCE_LAST_UPDATED} months and is being closed out. If you feel that this issue has been wrongly closed, please re-open the issue and tag \`@laurenbarker\` or \`@fqureshi\`.`;
        await addCommentAndCloseIssue(
          issueIid,
          comment,
          DEX_SERVICE_READ_WRITE_TOKEN,
        );
      }
    } else {
      // eslint-disable-next-line no-console
      console.error('Failed to fetch issues. Status:', response.status);
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Error:', error.message);
  }
}

async function addCommentAndCloseIssue(issueIid, comment, token) {
  const options = {
    headers: {
      'PRIVATE-TOKEN': token,
      'Content-Type': 'application/json',
    },
  };
  try {
    const data = {
      body: comment,
    };
    await axios.post(
      `${API_URL}/projects/${PROJECT_ID}/issues/${issueIid}/notes`,
      data,
      options,
    );

    await axios.put(
      `${API_URL}/projects/${PROJECT_ID}/issues/${issueIid}?state_event=close`,
      null,
      options,
    );
    // eslint-disable-next-line no-console
    console.log(`Comment added and issue ${issueIid} closed successfully.`);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(
      `Error adding comment and closing issue ${issueIid}:`,
      error.code,
      `${error.response.status} - ${error.response.statusText}`,
    );
  }
}

closeInactiveIssues();
